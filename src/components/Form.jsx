import React, {Component} from 'react';
// import PropTypes from 'prop-types';


class Form extends Component {
    state = {
        scoreText: '',
        errorScore: ''
    };

    handelInputScoreChange = ({target: {value}}) => {
        let error = '';
        const initialScore = '40802';
        console.log();
        const checkValidScore = (value.length <= 5 && value[[value.length - 1]] !== initialScore[value.length - 1]) || (value.length > 5 && !/^40802/.test(value));

        if (checkValidScore) {
            error = 'номер счета ИП должен начинается со следующих цифр 40802  вы неправильно выли ' + value;
            // value = 40802;
        } else {
            error = '';
        }

        this.setState({
            scoreText: value,
            errorScore: error
        })
    };

    render() {
        const {scoreText, errorScore} = this.state;

        return (
            <div className={`container`}>
                <div className={`row`}>
                    <div className={`form col-12`}>
                        <form action="">
                            <input type="text" name={`Bik`} placeholder='Бик' className={`form-control`}/>
                            <br/>
                            <input type="text" name={`Kor account`} className={`form-control`} placeholder='Кор счёт'/>
                            <br/>
                            <input type="text"
                                   name={`score`}
                                   className={`form-control`}
                                   placeholder='Расчётный счёт'
                                   value={scoreText}
                                   onChange={this.handelInputScoreChange}/>
                            <span className={`error text-danger`}>{errorScore}</span>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Form;